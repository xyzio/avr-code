/*
* two_driver_board.c
*
* Created: 4/14/2014 6:47:19 PM
*  Author: mkapoor
*/

#include "two_driver_board.h"

//TODO: Fix buzzer circuit


/*
1 - C7  - OPT1
2 - A5  - DRV1_A
3 - A7  - OPT2
4 - A4  - DRV1_B
5 - A6  - B1
6 - A3  - DRV1_C
7 - A0  - LED2
8 - A2  - DRV1_D
9 - A1  - B2
10 - C6 - LED1
*/

//Interrupt
//Remember:
//We write to PORTx
//We read from PINx

ISR(TIMER0_OVF_vect) {
	counter += 1;
}

ISR ( USART_RXC_vect )
{
	PORTD ^= (1 << PD6);
	
	ReceivedByte = UDR ; // Fetch the received byte value into the variable " ByteReceived "
	UDR = ReceivedByte ; // Echo back the received byte back to the computer
}


uint8_t remap(uint8_t digit) {
	//Remap digit to actual number
	if (digit == 0)
		return 0;
	if (digit == 1)
		return 8;
	if (digit == 2)
		return 4;
	if (digit == 3)
		return 12;
	if (digit == 4)
		return 2;
	if (digit == 5)
		return 10;
	if (digit == 6)
		return 6;
	if (digit == 7)
		return 14;
	if (digit == 8)
		return 1;
	if (digit == 9)
		return 9;

	return 15;
}

//map:
//In -> tube
//0 -> 0
//1 -> 8
//2 -> 4
//3 -> blank
//4 -> 2
//5 -> blank
//6 -> 6
//7 -> blank
//8 -> 1
//9 -> 9
//10 -> 5
//11 -> blank
//12 -> 3
//13 -> blank
//14 -> 7
//15 -> blank


void display(void) {

	if ((counter % 10) == 0) {


		displayWaitCounter += 1; //we could key off main counter variable


		//if (0 == (displayWaitCounter % waitDisplayTicks)) {


		if (digit0_on == currentDisplayState) {  //digit0 is on
			//turn off digit0
			//left bank
			PORTC |=  (1 << PC7); //Disable OPT1

			//right bank
			PORTC |= (1 << PC4); //Disable opt4.  PC4 == 1

			currentDisplayState = digit0_off;
			return;
		}
		else if (digit1_on == currentDisplayState) {
			//turn off digit 1
			//left bank
			PORTA |=  (1 << PA7); //Disable OPT2 [1000 0000]

			//right bank
			PORTC |= (1 << PC2); //Disable opt3

			currentDisplayState = digit1_off;
			return; //wait
		}
		else if (0 == (displayWaitCounter % waitDisplayTicks)) {


			UpdateLeftRight();

			if (digit0_off == currentDisplayState) { //We waited and returned. Digit0 is off, turn on digit1
				//UpdateLeftRight();
				//turn on digit1
				//left bank
				PORTA = (PINA & 0xC3) | ((remap(left & 0x0F)) << 2);     //1100 0011 (Clear and set A5-A2)
				PORTA &= ~(1 << PA7); //Enable OPT2

				//right bank
				volatile int nRemap = remap((right & 0xF0) >> 4) << 4;
				//Set DRVA
				PORTD = (PIND & 0x7F) | (nRemap & 0x80); //MSB of minutes x000 0000
				//Set DRVB
				PORTC = (PINC & 0xDF) /*00x0 0000*/ | ((nRemap & 0x40) /* 0x00 0000 */ >> 1);
				//Set DRVC, DRVD
				PORTD = (PIND & 0xCF) /* 00xx 0000 */ | ((nRemap & 0x30) /* 00xx 0000 */);
				PORTC &= ~(1 << PC2); //Enable opt3. PC2 == 0

				currentDisplayState = digit1_on; //digit1 is on.
				return; //Wait.
			}

			else if (digit1_off == currentDisplayState) {
				//UpdateLeftRight();
				//turn on digit0

				//Left bank
				//Enable OPT1 only if the MSB digit is non-0. This prevents ghosting and looks better
				if (0xF0 != (left & 0xF0)) {
					PORTA = (PINA & 0xC3) | ((remap((left & 0xF0) >> 4)) << 2);     //1100 0011 (Clear and set A5-A2)
					PORTC &= ~(1 << PC7); //Enable OPT1
				}

				//right bank
				volatile int nRemap = remap((right & 0x0F));
				//Set DRVA
				PORTD = (PIND & 0x7F) /*x000 0000*/ | ((nRemap & 0x08) << 4); //LSB of minutes 0000 x000
				//Set DRVB
				PORTC = (PINC & 0xDF) /*00x0 0000*/ | ((nRemap & 0x04) /* 0000 0x00 */ << 3);
				//Set DRVC, DRVD
				PORTD = (PIND & 0xCF) /*00xx 0000*/ | ((nRemap & 0x03) /* 0000 00xx */ << 4);

				PORTC &= ~(1 << PC4); //Enable opt4

				currentDisplayState = digit0_on;
				return;
			}
		}




		//UpdateLeftRight();
		//temp = (PINA & (1 << PA7)); //Check if OPT1 is disabled. 1 means it is disabled
		//
		//if (temp) {
		//
		//PORTC |=  (1 << PC7); //Disable OPT1
		//_delay_ms(5);
		//
		//PORTA = (PINA & 0xC3) | ((remap(left & 0x0F)) << 2);     //1100 0011 (Clear and set A5-A2)
		//
		//PORTA &= ~(1 << PA7); //Enable OPT2
		//}
		//else {
		//PORTA |=  (1 << PA7); //Disable OPT2 [1000 0000]
		//_delay_ms(5);
		//
		//PORTA = (PINA & 0xC3) | ((remap((left & 0xF0) >> 4)) << 2);     //1100 0011 (Clear and set A5-A2)
		//
		//PORTC &= ~(1 << PC7); //Enable OPT1
		//}
		//
		////minutes
		////opt3 = C2
		////opt4 = C4
		////DRVA = D7
		////DRVB = C5
		////DRVC = D5
		////DRVD = D4
		//
		////UpdateLeftRight();
		//temp = (PINC & (1 << PC2));
		//
		//if (temp) {
		////opt3 disabled. PC2 == 1
		//PORTC |= (1 << PC4); //Disable opt4.  PC4 == 1
		//_delay_ms(5);
		//
		//volatile int nRemap = remap((right & 0xF0) >> 4) << 4;
		////Set DRVA
		//PORTD = (PIND & 0x7F) | (nRemap & 0x80); //MSB of minutes x000 0000
		////Set DRVB
		//PORTC = (PINC & 0xDF) /*00x0 0000*/ | ((nRemap & 0x40) /* 0x00 0000 */ >> 1);
		////Set DRVC, DRVD
		//PORTD = (PIND & 0xCF) /* 00xx 0000 */ | ((nRemap & 0x30) /* 00xx 0000 */);
		//
		//
		//PORTC &= ~(1 << PC2); //Enable opt3. PC2 == 0
		//
		//}
		//else {
		////opt3 enabled. PC2 == 0. Cannot assume opt4 disable but is probably disabled.
		//PORTC |= (1 << PC2); //Disable opt3
		//_delay_ms(5);
		//
		//volatile int nRemap = remap((right & 0x0F));
		////Set DRVA
		//PORTD = (PIND & 0x7F) /*x000 0000*/ | ((nRemap & 0x08) << 4); //LSB of minutes 0000 x000
		////Set DRVB
		//PORTC = (PINC & 0xDF) /*00x0 0000*/ | ((nRemap & 0x04) /* 0000 0x00 */ << 3);
		////Set DRVC, DRVD
		//PORTD = (PIND & 0xCF) /*00xx 0000*/ | ((nRemap & 0x03) /* 0000 00xx */ << 4);
		//
		//PORTC &= ~(1 << PC4); //Enable opt4
		//}
	}
}



int main(void)
{
	//_delay_ms(1000);
	cli();
	setup();
	sei();

	while(1)
	{
	if (secs != prev_second) {
		prev_second = secs;
		//Blink LED
		//PORTC ^= (1 << PC6); //Toggle LED1
		//PORTA ^= (1 << PA0); //Toggle LED2

		//Change LEDs in 00, 01, 10, 11 sequence
		ledCounter += 1;
		PORTC = (PINC & ~(1 << PC6)) | ((ledCounter & 0x01) << PC6);
		PORTA = (PINA & ~(1 << PA0)) | (((ledCounter & 0x02) >> 1) << PA0);

		//Check for alarm. Should replace with buzzer when that is fixed.
		//the alarm variable will hold the true/false state for the buzzer.
		//Buzzer will not work at 1s interval
		if ((hour_alarm == hours) && (min_alarm == mins) && (mode != ALARMSET)) {
			PORTD ^= (1 << PD6);
		}

		//check for input
		//Check mode
		//B3 = C3 = S3
		temp = PINC & 0x08;  //0000 x000
		if (temp) {
			mode += 1;
			if (mode >= (NUMMODES)) {
				mode = 0;
			}
		}

		//Check buttons
		//B1 = A6 = S1
		temp = PINA & 0x40; //0x00 0000
		if (temp) {
			update_time(1);
		}

		//B2 = A1 = S2
		temp = PINA & 0x02; //0000 00x0
		if (temp) {
			update_time(0);
		}

		//Go through digits if it is a new minute
		if (timeUpdated == false) {
			if (anti_flashy == true) {
				if (anti_flashy_value < 9) {
					anti_flashy_value += 1;
				}
				else {
					anti_flashy = false;
				} //set anti-flashy to false if it is >= 10
			} //If anti-flashy is true
		} //If we are not updating time
	}

	//Go through digits if this is a new minute. This will prevent sputtering.
	if ((timeUpdated == false) && (mins != prev_minute)) {
		prev_minute = mins;
		anti_flashy = true;
		anti_flashy_value = 0;
	} //Once a minute stuff
	else if (timeUpdated == true) {
		prev_minute = mins;
		timeUpdated	= false;
		} //No anti-flashy on time update */

		display();

		if ((counter % 2048) == 0) {
		GetTimeDateAlarmDataFromRTC();
		}
	}
}

void setup(void) {

	//Disable JTAG port
	MCUCSR = (1<<JTD);
	MCUCSR = (1<<JTD);

	MCUCSR = (1<<JTD);
	MCUCSR = (1<<JTD);

	MCUCSR = (1<<JTD);
	MCUCSR = (1<<JTD);

	//Set up debug LED
	DDRD |= (1 << PD6);

	//Set up IO.
	//Hours
	DDRA |= (1 << PA0) | (1 << PA7) | (1 << PA2) | (1 << PA3) | (1 << PA4) | (1 << PA5);
	DDRC |= (1 << PC7) | (1 <<PC6);

	//minutes
	//opt3 = C2
	//opt4 = C4
	//DRVA = D7
	//DRVB = C5
	//DRVC = D5
	//DRVD = D4
	DDRC |= (1 << PC2) | (1 << PC4) | (1 << PC5);
	DDRD |= (1 << PD7) | (1 << PD5) | (1 << PD4);

	PORTC ^= (1 << PC6); //Toggle LED1

	//Interrupt
	TCCR0 |= (1 << CS00);  //Select regular clock
	TIMSK |= (1 << TOIE0); //Overflow interrupt enabled
	//
	init_rtc();
	
	//Serial
	UCSRB = (1 << RXEN ) | (1 << TXEN ); // Turn on the transmission and reception circuitry
	UCSRC = (1 << URSEL ) | (1 << UCSZ0 ) | (1 << UCSZ1 ); // Use 8- bit character sizes
	UBRRH = ( BAUD_PRESCALE >> 8) ; // Load upper 8- bits of the baud rate value into the high byte of the UBRR register
	UBRRL = BAUD_PRESCALE; // Load lower 8 - bits of the baud rate value into the low byte of the UBRR register
	UCSRB |= (1 << RXCIE ); // Enable the USART Recieve Complete interrupt ( USART_RXC )

}


void UpdateLeftRight(void) {
	//Set left/right based on which mode we are in.

	if (mode == TIME) {
		left  = hours;

		right = mins;
	} //Read time
	else if (mode == DATE) {
		left = month;
		right = day;
	} //Read date
	else if (mode == TEMP) {
		//Convert C to F
		int temperature = temp_MSB;
		temperature = (temperature << 1) + 32; /*<<1 to multiply by 2. Should be * 1.8 but this is faster */

		left   = DECTOBCD(((int)temperature));
		right  = DECTOBCD(((temperature - ((int)temperature)) * 100));

	} //Read temp
	else if (mode == ALARMSET) {
		//blink
		//uint8_t temp = 0;
		//temp = counter % 8;
		left = hour_alarm;
		right = min_alarm;
	} //Going to set alarm
	else if (mode == YEAR) {
		left = DECTOBCD(20);
		right = year;
	}  //year
	else if (mode == OFF) {
		left = right = 0xFF;
	}

	if (0 == (left & 0xF0)) {
		left = left | 0xF0;
	}  //If MSB is 0, then set MSB to off

	if (anti_flashy == true) {
		left = right = ((anti_flashy_value & 0x0F) << 4) | (anti_flashy_value & 0x0F);
	}
}

void update_time(uint8_t bank) {

	uint8_t temp = 0;

	//Values are stored as BCD
	if (mode == TIME) {
		if (bank) {
			//Convert hours from BCD to binary
			temp = BCDTODEC(hours);
			temp += 1;
			if (temp > 24) {
				temp = 1;
			}
			hours = DECTOBCD(temp);
			//Convert back to BCD
		} //left
		else {
			temp = BCDTODEC(mins);
			temp += 1;
			if (temp > 59) {
				temp = 0;
			}
			mins = DECTOBCD(temp);
		} //right
	}
	if (mode == DATE) {
		if (bank) {
			temp = BCDTODEC(month);
			temp += 1;
			if (temp > 12) {
				temp = 1;
			}
			month = DECTOBCD(temp);
		}
		else {
			temp = BCDTODEC(day);
			temp += 1;
			if (temp > 31) {
				temp = 1;
			}
			day = DECTOBCD(temp);
		}
	}

	if (mode == YEAR) {
		temp = BCDTODEC(year);
		if (bank) {
			temp += 1;
		}
		else {
			temp -= 1;
		}
		if (temp > 99) {
			temp = 0;
		}
		if (temp < 0) {
			temp = 0;
		}

		year = DECTOBCD(temp);

	}
	if (mode == ALARMSET) {
		if (bank) {
			temp = BCDTODEC(hour_alarm);
			temp += 1;
			if (temp > 24) {
				temp = 1;
			}
			hour_alarm = DECTOBCD(temp);
		}
		else {
			temp = BCDTODEC(min_alarm);
			temp += 1;
			if (temp > 59) {
				temp = 0;
			}
			min_alarm = DECTOBCD(temp);
		}
		sec_alarm = 0x80; //Set seconds to 0 and enable alarm when H:M:S match.
	}
	write_time();

	//Skip anti-flashy on update
	timeUpdated = true;
}

void GetTimeDateAlarmDataFromRTC(void) {

	//Get data we'll use later anyway
	twi_start(DS323X_ADDR_WR);
	twi_tx_byte(0x00); 					// address the beginning
	twi_start(DS323X_ADDR_RD); 				// now we are going to read from it
	// time
	secs = twi_rx_ack();
	mins = twi_rx_ack();
	hours = twi_rx_ack();
	// skip this one
	dow = twi_rx_ack(); 			// dummy read Day Of Week
	// date
	day = twi_rx_ack();
	month = twi_rx_ack();
	year = twi_rx_ack();
	//Alarm
	sec_alarm = twi_rx_ack();
	min_alarm = twi_rx_ack();
	hour_alarm = twi_rx_nack();
	twi_stop();

	twi_start(DS323X_ADDR_WR);
	twi_tx_byte(0x11);							// address the upper temperature byte
	twi_start(DS323X_ADDR_RD);
	temp_MSB = twi_rx_ack(); 				// read the temperature
	temp_LSB = twi_rx_nack(); 			// read 1/4 fraction
	twi_stop();




}  //Pull this out so we can update 1x second to reduce power consumption.

void write_time() {
	// now we need to send settings to DS323X RTC IC!
	twi_start(DS323X_ADDR_WR);
	twi_tx_byte(0x00); // address seconds
	twi_tx_byte(secs); // seconds=0
	twi_tx_byte(mins); // minutes
	twi_tx_byte(hours); // hours, but clear upper two bits
	twi_tx_byte(dow); // day of week - not used, so send 1
	twi_tx_byte(day); // day
	twi_tx_byte(month); // month, but clear the century bit
	twi_tx_byte(year); // year 00-99
	twi_tx_byte(sec_alarm); //alarm seconds
	twi_tx_byte(min_alarm);
	twi_tx_byte(hour_alarm);
	twi_stop();

	return;
}


//###########################
// Data conversion methods //
//###########################
// convert decimal to bcd
uint8_t DECTOBCD(uint8_t val)
{
	return ( (val/10) << 4 ) + ( val % 10 );
}
// convert bcd to decimal
uint8_t BCDTODEC(uint8_t val)
{
	return ( val>>4 ) * 10 + ( val & 0x0f );
}

//#################
// RTC functions //
//#################
// inits the DS323X IC and other things
void init_rtc()
{
	// set i2c for <= 400kHz SCL speed
	TWSR = I2C_TWSR_VAL;									// I2C prescaler value
	TWBR = I2C_TWBR_VAL;									// set 400khz I2C clock without prescaler

	// enable alarm 1
	//	twi_start(DS323X_ADDR_WR);
	//	twi_tx_byte(0x0E);										// address the Control Register
	//	twi_tx_byte(0x1D); 										// Enable alarm
	//	twi_tx_byte(0x89); 										// Enable alarm interrupt
	//	twi_stop();

	return;
}

//###############################
// TWI - I2C related functions //
//###############################
// start i2c
void twi_start(uint8_t addr)
{
	TWCR = ( 1<<TWEN ) | ( 1<<TWINT ) | ( 1<<TWSTA );

	while(!(TWCR & ( 1<<TWINT )));

	TWDR = addr;
	TWCR = ( 1<<TWEN ) | ( 1<<TWINT );

	while(!(TWCR & ( 1<<TWINT ))); 						// wait
}

// send a byte over i2c
void twi_tx_byte(uint8_t data)
{
	TWDR = data;
	TWCR = ( 1<<TWEN ) | ( 1<<TWINT );

	while(!(TWCR & ( 1<<TWINT ))); 						// wait
}

// stop i2c
void twi_stop(void)
{
	TWCR = ( 1<<TWEN ) | ( 1<<TWINT ) | ( 1<<TWSTO );
}

// receive byte + ack
uint8_t twi_rx_ack(void)
{
	TWCR = ( 1<<TWEN ) | ( 1<<TWINT ) | ( 1<<TWEA );

	while(!(TWCR & ( 1<<TWINT )));						// wait

	return TWDR;
}

// receive byte + nack
uint8_t twi_rx_nack(void)
{
	TWCR = ( 1<<TWEN ) | ( 1<< TWINT );

	while(!(TWCR & ( 1<<TWINT )));						// wait

	return TWDR;
}
