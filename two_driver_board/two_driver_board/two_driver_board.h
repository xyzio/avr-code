/*
* two_driver_board.h
*
* Created: 12/19/2014 1:26:26 PM
*  Author: mkapoor
*/


#ifndef TWO_DRIVER_BOARD_H_


#define F_CPU 8000000
#define TICK	1500 //Number of counters in 1 second
#define TICKRTC  5000 //RTC tick
#define false 0
#define true 1

//RTC
// Code macros
#define set0(port, pin)			( (port) &= (uint8_t)~_BV(pin) )
#define set1(port, pin)			( (port) |= (uint8_t)_BV(pin) )

// 32kHz / INT/SQW(-) input from DS323X IC
#define	SQW32KHZ_PIN		4
#define	SQW32KHZ_DDR		DDRB
#define	SQW32KHZ_PINREG		PINB
#define	SQW32KHZ_PORT		PORTB
#define	SQW32KHZ_PCINTBIT	PCINT4
#define	SQW32KHZ_PCMSKREG	PCMSK0
#define	SQW32KHZ_PCICRBIT	PCIE0

//I2C
#define	I2C_TWSR_VAL	0b00000000	// prescalerValue can be: 1,4,16,64
#define I2C_TWBR_VAL	6			// [7 = 400kHz @ 12MHz] by formula: CHOSEN_I2C_FREQ_HZ = F_CPU / ((16 + (2 * TWBR)) * prescalerValue)

// DS323X I2C Address
#define DS323X_ADDR		0xD0
#define DS323X_ADDR_RD		(DS323X_ADDR + 1)
#define DS323X_ADDR_WR		DS323X_ADDR

//Modes
#define TIME 		0
#define DATE 		1
#define TEMP 		2
#define ALARMSET 	3
#define YEAR		4
#define OFF			5
#define NUMMODES 	6

//Serial
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE ((( F_CPU / ( USART_BAUDRATE * 16UL ))) - 1)
volatile static char ReceivedByte;

#include <avr/io.h>
#include <avr/interrupt.h>
//#include <util/delay.h>

//functions
void setup(void);
void display(void);
void UpdateLeftRight(void);
void initialize(void);
void twi_start(uint8_t addr);
void twi_tx_byte(uint8_t data);
void twi_stop(void);
uint8_t twi_rx_ack(void);
uint8_t twi_rx_nack(void);
void init_rtc(void);
uint8_t DECTOBCD(uint8_t val);
uint8_t BCDTODEC(uint8_t val);
void write_time(void);
void update_time(uint8_t bank);
void GetTimeDateAlarmDataFromRTC(void);


static volatile uint16_t counter = 0;
static volatile uint16_t temp = 0;

volatile uint8_t left = 0;
volatile uint8_t right = 0;
volatile uint8_t anti_flashy = false;
volatile uint8_t anti_flashy_value = 0;
volatile uint8_t timeUpdated = false;

////Time variables
uint8_t secs;
uint8_t mins;
uint8_t hours;
// skip this one
uint8_t dow; 			// dummy read Day Of Week
// date
uint8_t day;
uint8_t month;
uint8_t year;

//alarm
uint8_t hour_alarm;
uint8_t min_alarm;
uint8_t sec_alarm;
uint8_t alarm = 0;

//temp
uint8_t temp_MSB = 0x11;
uint8_t temp_LSB = 0x11;

//Keeping track of seconds
uint8_t prev_second = 99;

//keeping track of minutes
uint8_t prev_minute = 99;

///End of time variables


//Begin of display variables
typedef enum  {
	digit0_off,
	digit0_on,
	digit1_off,
	digit1_on
} DisplayState;

volatile uint16_t waitDisplayTicks = 10; //Number of ticks to wait between display events

DisplayState currentDisplayState = digit0_on;
volatile uint16_t displayWaitCounter = 0;

volatile uint8_t ledCounter = 0;

volatile uint8_t mode = TIME;  //Mode is day, date, temp, alarm

#endif /* TWO_DRIVER_BOARD_H_ */