# README #

This is AVR source code featured on http://xyzio.com or http://blog.xyzio.com

## Code Description ##

_avr code/two_driver_board_: This is the code for a Nixie Tube clock.  Features include an alarm clock, current temperature, and battery backup.  Read more about it and see pictures on my blog - [https://blog.xyzio.com/2015/04/06/project-nixie-tube-clock/](https://blog.xyzio.com/2015/04/06/project-nixie-tube-clock/).

_avr code/characterOLED_: This is a slight tweak of Adafruit's Arduino OLED library. It omits the Arduino library which means it can run on an AVR without the Arudino framework.  Read about it and see pictures on my blog - [https://blog.xyzio.com/2014/12/26/non-arduino-adafruit-oled-display-library/](https://blog.xyzio.com/2014/12/26/non-arduino-adafruit-oled-display-library/).